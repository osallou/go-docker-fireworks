from fireworks import Firework, Workflow, LaunchPad, ScriptTask
from fireworks.core.rocket_launcher import rapidfire
from godocker_task import GoDockerTask

# set up the LaunchPad and reset it
launchpad = LaunchPad()
launchpad.reset('', require_password=False)

param = {
  'stdout_file': 'hello.out',
  'cpu': 2
}
task1 = GoDockerTask.from_str('echo "hello"', parameters=param)
# create the individual FireWorks and Workflow
fw1 = Firework(task1, name="hello")
fw2 = Firework(GoDockerTask.from_str('echo "goodbye"'), name="goodbye")
wf = Workflow([fw1, fw2], {fw1:fw2}, name="test workflow")

# store workflow and launch it locally
launchpad.add_wf(wf)
rapidfire(launchpad)
