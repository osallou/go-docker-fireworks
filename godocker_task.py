# coding: utf-8

from __future__ import unicode_literals

import shlex
import subprocess
import sys

from datetime import datetime
import time
import os
import multiprocessing
import logging
import json

from fireworks.core.firework import FireTaskBase, FWAction
from six.moves import builtins

from godockercli.auth import Auth
from godockercli.utils import Utils
from godockercli.httputils import HttpUtils

__author__ = 'Olivier Sallou'
__copyright__ = 'Copyright 2015, GenOuest BioInformatics Platform'
__version__ = '0.1'
__maintainer__ = 'Olivier Sallou'
__email__ = 'olivier.sallou@irisa.fr'
__date__ = 'Jun 30, 2015'

from fireworks.utilities.fw_utilities import explicit_serialize

@explicit_serialize
class GoDockerTask(FireTaskBase):
    required_params = ["script"]
    #_fw_name = 'GoDockerTask'


    def run_task(self, fw_spec):
        if self.get("use_global_spec"):
            self._load_params(fw_spec)
        else:
            self._load_params(self)

        try:
            Auth.authenticate()
        except Exception:
            raise RuntimeError('GoDockerTask: wrong authentication')

        return self._run_task_internal(fw_spec)

    def _get_god_file(self, task_id, file_name):
        file_content = HttpUtils.http_get_request(
                "/api/1.0/task/"+str(task_id)+"/files/"+file_name,
                Auth.server,
                {'Authorization':'Bearer '+Auth.token},
                Auth.noCert
        )
        return file_content.text

    def _run_task_internal(self, fw_spec):
        login = Auth.login
        user_infos = Utils.get_userInfos(login)
        dt = datetime.now()
        # run the program
        #stdout = subprocess.PIPE if self.store_stdout or self.stdout_file else sys.stdout
        #stderr = subprocess.PIPE if self.store_stderr or self.stderr_file else sys.stderr
        job = {
                'user' : {
                    'id' : user_infos['id'],
                    'uid' : user_infos['uid'],
                    'gid' : user_infos['gid']
                },
                'date': time.mktime(dt.timetuple()),
                'meta': {
                    'name': 'fireworksWorflowTask',
                    'description': 'fireworks workflow',
                    'tags': ['fireworks']
                },
                'requirements': {
                    'cpu': self.cpu,
                    # In Gb
                    'ram': self.ram,
                    'label': [],
                    'array': { 'values': None}
                },
                'container': {
                    'image': self.image,
                    'volumes': self.volumes,
                    'network': True,
                    'id': None,
                    'meta': None,
                    'stats': None,
                    'ports': [],
                    'root': self.is_root
                },
                'command': {
                    'interactive': False,
                    'cmd': self.script,
                },
                'status': {
                    'primary': None,
                    'secondary': None
                }

            }

        result_submit = HttpUtils.http_post_request(
                "/api/1.0/task", json.dumps(job),
                Auth.server,
                {'Authorization':'Bearer '+Auth.token, 'Content-type': 'application/json', 'Accept':'application/json'},
                Auth.noCert
        )
        job_id = result_submit.json()['id']
        self.task_id = job_id
        not_over = True
        state = False
        job = None
        while not_over:
            task = HttpUtils.http_get_request(
                    "/api/1.0/task/"+str(job_id),
                    Auth.server,
                    {'Authorization':'Bearer '+Auth.token},
                    Auth.noCert
            )

            job=task.json()
            if job['status']['primary'] == 'over':
                if job['status']['secondary'] is None or not job['status']['secondary']:
                    state = True
                else:
                    state = False
                not_over = False
            time.sleep(1)

        returncode = job['container']['meta']['State']['ExitCode']

        godout = self._get_god_file(self.task_id, 'god.log')
        goderr = self._get_god_file(self.task_id, 'god.err')

        if self.stdout_file:
            with open(self.stdout_file, 'a+') as f:
                f.write(godout)

        if self.stderr_file:
            with open(self.stderr_file, 'a+') as f:
                f.write(goderr)

        # write the output keys
        output = {}

        if self.store_stdout:
            output['stdout'] = godout

        if self.store_stderr:
            output['stderr'] = goderr

        output['returncode'] = returncode
        output['all_returncodes'] = returncode
        output['task_id'] = job['id']

        if self.defuse_bad_rc and not state:
            return FWAction(stored_data=output, defuse_children=True)

        elif self.fizzle_bad_rc and not state:
            raise RuntimeError('goDockerTask fizzled! Return code: {}'.format(returncode))

        return FWAction(stored_data=output)

    def _load_params(self, d):
        self.use_shell = d.get('use_shell', True)

        self.script = d['script']
        self.cpu = d['cpu']
        self.ram = d['ram']
        self.image = d['image']
        self.is_root = d['is_root']
        self.volumes = d['volumes']

        self.stdout_file = d.get('stdout_file')
        self.stderr_file = d.get('stderr_file')
        self.store_stdout = d.get('store_stdout')
        self.store_stderr = d.get('store_stderr')
        self.shell_exe = d.get('shell_exe')
        self.defuse_bad_rc = d.get('defuse_bad_rc')
        self.fizzle_bad_rc = d.get('fizzle_bad_rc', not self.defuse_bad_rc)

        if self.defuse_bad_rc and self.fizzle_bad_rc:
            raise ValueError("ScriptTask cannot both FIZZLE and DEFUSE a bad returncode!")

    @classmethod
    def from_str(cls, shell_cmd, parameters=None):
        parameters = parameters if parameters else {}
        parameters['script'] = shell_cmd
        parameters['use_shell'] = True
        if 'cpu' not in parameters:
            parameters['cpu'] = 1
        if 'ram' not in parameters:
            parameters['ram'] = 1
        if 'image' not in parameters:
            parameters['image'] = 'centos'
        if 'is_root' not in parameters:
            parameters['is_root'] = False
        if 'volumes' not in parameters:
            parameters['volumes'] = []
        return cls(parameters)
