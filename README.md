# GoDocker task for FireWorks

FireWorks is a free, open-source code for defining, managing, and executing
scientific workflows. It can be used to automate calculations over arbitrary
computing resources, including those that have a queueing system.

FireWorks: https://pythonhosted.org/FireWorks

#Task

godocker_task.py provides a GoDockerTask for FireWorks to execute a bash script
on go-docker.

You need to authenticate first with godocker using the godocker_cli package and
the godlogin command. The GoDockerTask will use those credentials to connect to
the remote godocker server.

An example workflow test.py is available to show usage.

Parameters can define stdout_file, stderr_file as well as cpu, ram, image,
is_root and volumes (array of godocker volumes to mount). Example:

    param = {
        'stdout_file': 'hello.out',
        'cpu': 2
    }
    task1 = GoDockerTask.from_str('echo "hello"', parameters=param)

